//
//  YSBaseNavigationController.h
//  iOS-Learning-Demo
//
//  Created by yuan on 2019/2/22.
//  Copyright © 2019 yuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RTRootNavigationController/RTRootNavigationController.h>

NS_ASSUME_NONNULL_BEGIN

@interface YSBaseNavigationController : RTRootNavigationController

@property(nonatomic,assign) NSInteger barStyle;

@end

NS_ASSUME_NONNULL_END
