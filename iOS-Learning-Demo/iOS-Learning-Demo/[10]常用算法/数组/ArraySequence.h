//
//  ArraySequence.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/29.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArraySequence : NSObject

#pragma mark - 1、删除排序数组中的重复项
+ (void)removeDuplicates;

#pragma mark - 2、买卖股票的最佳时机2
+ (void)maxProfit;

#pragma mark - 3、旋转数组
+ (void)rotate;

#pragma mark - 4、存在重复元素
+ (void)containsDuplicates;

#pragma mark - 5、只出现一次的数字
+ (void)singleNumber;

#pragma mark - 6、两个数组的交集 II
+ (void)intersect;

//TODO:找出数组中重复的数字
+ (void)findDuplicates;

//TODO:二维数组中的查找
//在一个二位数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
//请完成一个函数，输入这样的一个二位数组和一个整数，判断数组中是否含有该整数
+ (void)findMatrix;

//TODO:求出这组数字子序列和中最大值
+ (void)findMaxNums;

//TODO:合并两个有序数列
+ (void)combinSortArr;

@end

NS_ASSUME_NONNULL_END
