//
//  ListNodeSequence.m
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/29.
//  Copyright © 2022 yuan. All rights reserved.
//

#import "ListNodeSequence.h"
#import "ListNode.h"

@implementation ListNodeSequence

//TODO:从尾到头打印链表
//输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
+ (void)reversePrint{
    ListNode *node;
    node = [ListNode addNode:node andVlaue:@(1)];
    node = [ListNode addNode:node andVlaue:@(3)];
    node = [ListNode addNode:node andVlaue:@(2)];
    
    NSArray *nodeArr = [ListNode getLinkList:node];
    [nodeArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSLog(@"%@",obj);
    }];
    
    node = [ListNode reverseList:node];
    nodeArr = [ListNode getLinkList:node];
    NSLog(@"单链表逆置 ---> %@",nodeArr);
}


@end
