//
//  CharSequence.m
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/29.
//  Copyright © 2022 yuan. All rights reserved.
//

#import "CharSequence.h"

@implementation CharSequence

//TODO:替换空格
//请实现一个函数，把字符串 中的每个空格替换成"%20"。
+ (void)replaceSpace{
    NSString *str = @"We are happy.";
    NSLog(@"输入：%@",str);
    str = [self replaceSpace3:str];
    NSLog(@"输出：%@",str);
}

//系统函数
+ (NSString *)replaceSpace1:(NSString *)string{
    return [string stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

//比较每个字符串，替换
+ (NSString *)replaceSpace2:(NSString *)string{
    for (int i = 0; i<[string length]; i++) {
        NSString *s = [string substringWithRange:NSMakeRange(i, 1)];
        if ([s isEqualToString:@" "]) {
            NSRange range = NSMakeRange(i, 1);
            string = [string stringByReplacingCharactersInRange:range withString:@"%20"];
        }
    }
    return string;
}

//拼接字符串
+ (NSString *)replaceSpace3:(NSString *)string{
    NSMutableString *resultStr = @"".mutableCopy;
    for (int i = 0; i<[string length]; i++) {
        NSString *s = [string substringWithRange:NSMakeRange(i, 1)];
        if ([s isEqualToString:@" "]) {
            [resultStr appendString:@"%20"];
        }else{
            [resultStr appendString:s];
        }
    }
    return resultStr;
}


@end
