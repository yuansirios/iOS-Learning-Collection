//
//  CharSequence.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/29.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CharSequence : NSObject

//TODO:替换空格
//请实现一个函数，把字符串 中的每个空格替换成"%20"。
+ (void)replaceSpace;

@end

NS_ASSUME_NONNULL_END
