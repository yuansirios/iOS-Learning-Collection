//
//  FibonacciSequence.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/21.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FibonacciSequence : NSObject

/**
 基本循环

 @param number n
 @return 斐波那契数值
 */
+ (NSInteger)normalFibonacci:(NSInteger)number;

/**
 递归

 @param number n
 @return 斐波那契数值
 */
+ (NSInteger)recursionFibonacci:(NSInteger)number;

/**
 递归记忆

 @param number n
 @return 斐波那契数值
 */
+ (NSInteger)memoryRecursionFibonacci:(NSInteger)number;

@end

NS_ASSUME_NONNULL_END


