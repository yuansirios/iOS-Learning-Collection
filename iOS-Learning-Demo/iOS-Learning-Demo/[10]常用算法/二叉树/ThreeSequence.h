//
//  ThreeSequence.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/29.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThreeSequence : NSObject

//TODO:重建二叉树
/**
 
 输入某二叉树的前序遍历和中序遍历的结果，请重建该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 输入：
 
 前序遍历 preorder = [3,9,20,15,7]
 中序遍历 inorder = [9,3,15,20,7]
 
 3
/ \
9  20
 /  \
15   7

限制
0 <= 节点个数 <= 5000
 */

+ (void)buildTree;

@end

NS_ASSUME_NONNULL_END
