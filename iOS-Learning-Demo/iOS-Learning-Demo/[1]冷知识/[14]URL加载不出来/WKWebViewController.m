//
//  WKWebViewController.m
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2021/10/22.
//  Copyright © 2021 yuan. All rights reserved.
//

#import "WKWebViewController.h"
#import <WebKit/WebKit.h>

@interface WKWebViewController ()<WKNavigationDelegate,WKUIDelegate>

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, strong) UIWebView *oldWebView;

@property (nonatomic, strong) WKWebViewConfiguration *wkConfig;

@end

@implementation WKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *url = @"http://192.168.226.61:8000/mobile/link-share?linkId=REPORT_LINK_4bc6c48f26e4d1f85";
    
    url = @"http://192.168.226.61:8000/share?linkId=REPORT_LINK_4bc6c48f26e4d1f85&lang=zh_CN&reportFilter=[]&chartFilter=[]";
    
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height) configuration:self.wkConfig];
    self.webView.backgroundColor = UIColor.whiteColor;
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    [self.view addSubview:self.webView];

    [self webViewURL:[NSURL URLWithString:url]];
    
//    self.oldWebView = [[UIWebView alloc]initWithFrame:self.view.bounds];
//    [self.view addSubview:self.oldWebView];
//    [self.oldWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
//
    
}


#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSURL *URL = navigationAction.request.URL;
    NSLog(@"URL:%@",URL);
    //允许页面跳转
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webViewURL:(NSURL *)url{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"WebView加载失败:%@",error);
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"WebView加载失败:%@",error);
}

- (WKWebViewConfiguration *)wkConfig {
    if (!_wkConfig) {
        _wkConfig = [[WKWebViewConfiguration alloc] init];
        _wkConfig.allowsInlineMediaPlayback = YES;
        if (@available(iOS 9.0, *)) {
            _wkConfig.allowsPictureInPictureMediaPlayback = YES;
        }
//        [BLWebUrlParseUtil setViewPortDefault:_wkConfig];
    }
    return _wkConfig;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
