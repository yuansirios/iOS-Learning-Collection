//
//  CustomPushViewController.m
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/2/8.
//  Copyright © 2022 yuan. All rights reserved.
//

#import "CustomPushViewController.h"

@interface CustomPushViewController ()

@end

@implementation CustomPushViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"push自定义转场";
}

- (void)onBack{
    CATransition * transition = [CATransition animation];

    transition.duration = 0.4f;

    transition.type = kCATransitionReveal;

    transition.subtype = kCATransitionFromBottom;

    [self.rt_navigationController.view.layer addAnimation:transition forKey:kCATransition];

    [self.rt_navigationController popViewControllerAnimated:NO];
}

@end
