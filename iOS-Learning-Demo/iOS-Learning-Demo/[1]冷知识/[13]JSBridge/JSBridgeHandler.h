//
//  JSBridgeHandler.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2021/9/23.
//  Copyright © 2021 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^JSBridgeHandlerBlock)(NSString *scriptStr);

@interface JSBridgeHandler : NSObject<WKScriptMessageHandler>

@property (nonatomic, copy) JSBridgeHandlerBlock handleBlock;

@end

NS_ASSUME_NONNULL_END
