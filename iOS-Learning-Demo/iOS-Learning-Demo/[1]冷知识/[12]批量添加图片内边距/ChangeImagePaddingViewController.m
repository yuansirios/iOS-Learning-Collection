//
//  ChangeImagePaddingViewController.m
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2021/9/17.
//  Copyright © 2021 yuan. All rights reserved.
//

#import "ChangeImagePaddingViewController.h"

@interface ChangeImagePaddingViewController ()

@property (nonatomic, strong) UIImageView *baseImageView;

@end

@implementation ChangeImagePaddingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改图片内边距";
    
    self.baseImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img2"]];
    self.baseImageView.frame = CGRectMake(0, 100, 100, 100);
    self.baseImageView.backgroundColor = UIColor.blackColor;
    self.baseImageView.centerX = self.view.centerX;
    [self.view addSubview:self.baseImageView];
    
    UIButton *btn = UIButton.new;
    [btn setTitle:@"转换" forState:UIControlStateNormal];
    btn.backgroundColor = UIColor.greenColor;
    btn.frame = CGRectMake(0, self.baseImageView.bottom + 40, 100, 40);
    btn.centerX = self.view.centerX;
    [btn addTarget:self action:@selector(changePlistSource) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

//替换国际化词条
- (void)changePlistSource{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"BLFaceDefaultIconEmoji" ofType:@"plist"];
    NSDictionary *fileDic = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"emoji" ofType:@"json"];
    NSError *error;
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:jsonPath];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    NSArray *icons = [fileDic arrayForKey:@"icons"];
    
    NSMutableArray *updateIcons = @[].mutableCopy;
    
    for (NSDictionary *dic in icons){
        NSString *code = [dic stringForKey:@"code"];
        
        NSMutableDictionary *nameDic = [dic dictionaryForKey:@"name"].mutableCopy;
        
        NSLog(@"----- %@ ----",code);
        
        NSMutableDictionary *codeDic = [jsonDic dictionaryForKey:code].mutableCopy;
        
        for (NSString *nameKey in nameDic.allKeys){
            if ([codeDic.allKeys containsObject:nameKey]){
                NSLog(@"%@ | %@",nameKey,codeDic[nameKey]);
                NSString *value = codeDic[nameKey];
                value = [value stringByReplacingOccurrencesOfString:@"[" withString:@""];
                value = [value stringByReplacingOccurrencesOfString:@"]" withString:@""];
                nameDic[nameKey] = value;
            }else{
                NSLog(@"%@ | %@",nameKey,nameDic[nameKey]);
            }
            //西语用英文
            if ([nameKey isEqualToString:@"es"]){
                nameDic[nameKey] = nameDic[@"en"];
            }
        }
        
        NSMutableDictionary *itemDic = @{}.mutableCopy;
        [itemDic setValue:nameDic forKey:@"name"];
        [itemDic setValue:code forKey:@"code"];
        [itemDic setValue:[NSString stringWithFormat:@"BLFaceDefaultIconEmoji.bundle/%@",code] forKey:@"image"];
        
        [updateIcons addObject:itemDic];
    }
    
    NSMutableDictionary *plistDic = @{}.mutableCopy;
    [plistDic setString:@"BLFaceDefaultIconEmoji.bundle/icon" forKey:@"image"];
    [plistDic setString:@"默认" forKey:@"name"];
    [plistDic setString:@"local" forKey:@"Id"];
    [plistDic setValue:updateIcons forKey:@"icons"];
    
    [plistDic writeToFile:@"/Users/yuan/Desktop/BLFaceDefaultIconEmoji.plist" atomically:YES];
}

- (void)changeInsert{
    
    for (int i = 1 ; i < 103 ; i++){
        NSString *fileName = [NSString stringWithFormat:@"emoji_%02d",i];
        
        NSString *path = [NSString stringWithFormat:@"/Users/yuan/Desktop/imgs/%@.png",fileName];
        
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"png"];

        UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
        
        img = [self changeImage:img size:CGSizeMake(64, 64)];
        
        NSLog(@"图片大小：%@",NSStringFromCGSize(img.size));
        
        [UIImagePNGRepresentation(img) writeToFile:path atomically:YES];
        
        self.baseImageView.image = img;
    }
    
}

- (UIImage *)changeImage:(UIImage *)image size:(CGSize)size{
    float padding = 5;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(padding, padding, size.width - padding*2, size.height - padding*2)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
