//
//  BLAsyncBlockOperation.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/3/14.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BLAsyncBlockOperation;

typedef void (^BLAsyncBlock)(BLAsyncBlockOperation * __nonnull asyncOperation);

@interface BLAsyncBlockOperation : NSOperation

- (nonnull instancetype)initWithBlock:(nonnull BLAsyncBlock)block;
+ (nonnull instancetype)blockOperationWithBlock:(nonnull BLAsyncBlock)block;
- (void)complete;

@end

NS_ASSUME_NONNULL_END
