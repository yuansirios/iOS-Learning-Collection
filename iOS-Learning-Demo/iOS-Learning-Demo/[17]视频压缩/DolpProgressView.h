//
//  DolpProgressView.h
//  iOS-Learning-Demo
//
//  Created by yuan xiaodong on 2022/1/5.
//  Copyright © 2022 yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DolpProgressView : UIView

@property (nonatomic, assign) double progress;

@end

NS_ASSUME_NONNULL_END
